#pragma once
#include <Adafruit_ILI9341.h>
#include <Adjuster.h>



class Adjuster {
private:
    int16_t _xAnchor;
    int16_t _yAnchor;
    int16_t _width;
    int16_t _height;
    int *_control_variable;
    int _step_size = 1;
public:
    Adjuster();
    Adjuster (int *control_var, int16_t x, int16_t y);
    int16_t GetX() { return _xAnchor; }
    int16_t GetY() { return _yAnchor; }
    int16_t GetHeight() { return _height; }
    int16_t GetWidth() { return _width; }
    void Show(Adafruit_ILI9341 tft);
    bool ContainsPoint(int16_t x, int16_t y);
    void Press(int16_t x, int16_t y);
    void ChangeLabel(char* new_label);
};
