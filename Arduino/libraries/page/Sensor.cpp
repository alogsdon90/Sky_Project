#pragma once
#include <Adafruit_ILI9341.h>
#include "Sensor.h"

Sensor::Sensor(){

}

Sensor::Sensor (int pin_num, String label, int16_t x, int16_t y){
    _pin_num = pin_num;
    _xAnchor = x;
    _label = label;
    _yAnchor = y;
    _width = 50;
    _height = 35;
    _color = ILI9341_DARKCYAN;
}


void Sensor::Show(Adafruit_ILI9341 tft){
    tft.fillRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, this->_color);
    tft.setCursor(this->_xAnchor + 6 , this->_yAnchor + (this->_height/2));
    tft.setTextColor(ILI9341_WHITE);
    tft.setTextSize(2);
    uint16_t val = this->Read();
    tft.println(String(val)+=this->_label); //.concat(this->_label)
    tft.drawRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, ILI9341_BLACK);
}



void Sensor::ChangeLabel(char* new_label){

}

void Sensor::ChangePin(int pin_num){

}