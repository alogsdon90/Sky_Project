#pragma once
#include <Adafruit_ILI9341.h>


class Sensor {
private:
    int16_t _xAnchor;
    int16_t _yAnchor;
    int16_t _width;
    int16_t _height;
    String _label;
    int _pin_num;
    uint16_t _color;
    
public:
    Sensor();
    Sensor (int pin_num, String label, int16_t x, int16_t y);
    int16_t GetX() { return _xAnchor; }
    int16_t GetY() { return _yAnchor; }
    int16_t GetHeight() { return _height; }
    int16_t GetWidth() { return _width; }
    uint16_t Read() { return analogRead(_pin_num); }
    void Show(Adafruit_ILI9341 tft);
    void ChangeLabel(char* new_label);
    void ChangePin(int pin_num);
};