
#pragma once
#include "page.h"
#include <Adafruit_ILI9341.h>




Page::Page() {
    this->_num_buttons = 0;
    this->_num_labels = 0;
    this->_num_adj = 0;
    this->_num_sens = 0;
}

void Page::Press(int x, int y, Adafruit_ILI9341 tft) {
    for (int16_t i = 0; i < this->_num_buttons; i++) {
        if (this->_buttons[i].ContainsPoint(x,y)) {
            this->_buttons[i].CallFunction();
            return;
        }
    }
    for (int16_t i = 0; i < this->_num_adj; i++) {
        if (this->_adj[i].ContainsPoint(x, y)) {
            this->_adj[i].Press(x, y);
            this->_adj[i].Show(tft);
            return;
        }
    }
}

void Page::Show(Adafruit_ILI9341 tft) {


    tft.fillScreen(ILI9341_LIGHTGREY);

    for (int16_t i = 0; i < this->_num_buttons; i++) {
        this->_buttons[i].Show(tft);
    }

    for (int16_t i = 0; i < this->_num_labels; i++) {
        this->_labels[i].Show(tft);
    }
    for (int16_t i = 0; i < this->_num_adj; i++) {
        this->_adj[i].Show(tft);
    }
    for (int16_t i = 0; i < this->_num_sens; i++) {
        this->_sens[i].Show(tft);
    }
}

void Page::Update(Adafruit_ILI9341 tft) {
    for (int16_t i = 0; i < this->_num_sens; i++) {
        this->_sens[i].Show(tft);
    }
    for (int16_t i = 0; i < this->_num_labels; i++) {
        if (!this->_labels[i].isStatic())
            this->_labels[i].Show(tft);
    }
}

void Page::AddButton(Button2 &b) {
    this->_buttons[this->_num_buttons] = b;
    this->_num_buttons++;
}

void Page::AddSensor(Sensor &s) {
    this->_sens[this->_num_sens] = s;
    this->_num_sens++;
}

void Page::AddLabel(Label2 l) {
    this->_labels[this->_num_labels] = l;
    this->_num_labels++;
}

void Page::AddAdjuster(Adjuster a) {
    this->_adj[this->_num_adj] = a;
    this->_num_adj++;
}

Button2::Button2 (char* label, int16_t x, int16_t y, void (*callback_func)(void)) {
    _label = label;
    _xAnchor = x;
    _yAnchor = y;
    _width = 100;
    _height = 50;
    _color = ILI9341_BLUE;
    _callback_func = callback_func;
    
}

void Button2::ChangeLabel(char* new_label) {
    _label = new_label;
}

Button2::Button2 () {
    _xAnchor = 0;
    _yAnchor = 0;
    _width = 1;
    _height = 1;
    _callback_func = []() { return; };
}

void Button2::Show(Adafruit_ILI9341 tft) {

    

    tft.fillRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, this->_color);
    tft.setCursor(this->_xAnchor + 6 , this->_yAnchor + (this->_height/2));
    tft.setTextColor(ILI9341_WHITE);
    tft.setTextSize(2);
    tft.println(this->_label);
    tft.drawRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, ILI9341_BLACK);
}

bool Button2::ContainsPoint(int x, int y) {
    return ( x >= this->_xAnchor && x <= (this->_xAnchor + this->_width)
    && y >= this->_yAnchor && y <= (this->_yAnchor + this->_height));

}

Label2::Label2() {
    _label = "";
    _xAnchor = 10;
    _yAnchor = 10;
    _width = 100;
    _height = 50;
    _stat = true;
    _color = ILI9341_GREENYELLOW;
}

Label2::Label2(char* label, int16_t x, int16_t y) {
    _label = label;
    _xAnchor = x;
    _yAnchor = y;
    _width = 80;
    _height = 50;
    _stat = true;
    _color = ILI9341_GREENYELLOW;
}

void Label2::Show(Adafruit_ILI9341 tft) {
    tft.fillRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, this->_color);
    tft.setCursor(this->_xAnchor + 6 , this->_yAnchor + (this->_height/2));
    tft.setTextColor(ILI9341_BLACK);
    tft.setTextSize(2);
    tft.println(this->_label);
    tft.drawRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, ILI9341_BLACK);
}