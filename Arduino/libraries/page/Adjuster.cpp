#pragma once
#include <Adjuster.h>


Adjuster::Adjuster(){

}
Adjuster::Adjuster (int *control_var, int16_t x, int16_t y){
    _control_variable = control_var;
    _xAnchor = x;
    _yAnchor = y;
    _width = 50;
    _height = 25;
}

void Adjuster:: Show(Adafruit_ILI9341 tft) {
    tft.fillRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, ILI9341_BLUE);
    tft.fillRect(this->_xAnchor, this->_yAnchor - this->_height, this->_width, this->_height, ILI9341_GREEN);
    tft.fillRect(this->_xAnchor, this->_yAnchor + this->_height, this->_width, this->_height, ILI9341_RED);
    tft.setCursor(this->_xAnchor + 6 , this->_yAnchor + (this->_height/3));
    tft.setTextColor(ILI9341_WHITE);
    tft.setTextSize(2);
    tft.println(*(this->_control_variable));
    tft.drawRect(this->_xAnchor, this->_yAnchor, this->_width, this->_height, ILI9341_BLACK);
    tft.drawRect(this->_xAnchor, this->_yAnchor - this->_height, this->_width, 3 * this->_height, ILI9341_BLACK);
}
bool Adjuster:: ContainsPoint(int16_t x, int16_t y){
    return ( x >= this->_xAnchor && x <= (this->_xAnchor + this->_width)
    && y >= this->_yAnchor-this->_height && y <= (this->_yAnchor + 2 * this->_height));
}

void Adjuster:: Press(int16_t x, int16_t y){
    if ( y >= this->_yAnchor + this->_height / 2) {
        //decrement
        *(this->_control_variable) -= this->_step_size;
    } else {
        //increment
        *(this->_control_variable) += this->_step_size;
    }
    delay(200);
}

void Adjuster:: ChangeLabel(char* new_label){

}