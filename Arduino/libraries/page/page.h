
#pragma once
#include <Adafruit_ILI9341.h>
#include "Adjuster.h"
#include "Sensor.h"



static const uint16_t MAX_BUTTONS = 5;

class Button2 {
private:
    int16_t _xAnchor;
    int16_t _yAnchor;
    int16_t _width;
    int16_t _height;
    uint16_t _color;
    char* _label;
    void (*_callback_func)(void);
public:
    Button2();
    Button2 (char* label, int16_t x, int16_t y, void (*callback_func)(void));
    int16_t GetX() { return _xAnchor; }
    int16_t GetY() { return _yAnchor; }
    int16_t GetHeight() { return _height; }
    int16_t GetWidth() { return _width; }
    void SetHeight(int16_t height) { this->_height = height; }
    void SetWidth(int16_t width) { this->_width = width; }
    void SetColor(uint16_t color) { this->_color = color; }
    void Show(Adafruit_ILI9341 tft);
    bool ContainsPoint(int x, int y);
    void CallFunction() { _callback_func();}
    void ChangeLabel(char* new_label);
};

class Label2 {
private:
    int16_t _xAnchor;
    int16_t _yAnchor;
    int16_t _width;
    int16_t _height;
    uint16_t _color;
    bool _stat;
    char* _label;
public:
    Label2();
    Label2 (char* label, int16_t x, int16_t y);
    int16_t GetX() { return _xAnchor; }
    int16_t GetY() { return _yAnchor; }
    int16_t GetHeight() { return _height; }
    int16_t GetWidth() { return _width; }
    void SetStatic(bool st) { _stat = st; }
    bool isStatic() { return _stat; }
    void SetHeight(int16_t height) { this->_height = height; }
    void SetWidth(int16_t width) { this->_width = width; }
    void SetLabel(char* label) { this->_label = label; }
    void SetColor(uint16_t color) { this->_color = color; }
    void Show(Adafruit_ILI9341 tft);
};

class Page
{
 private:
    Button2 _buttons[MAX_BUTTONS];
    Label2 _labels[MAX_BUTTONS];
    Adjuster _adj[MAX_BUTTONS];
    Sensor _sens[MAX_BUTTONS];
    uint16_t _num_buttons;
    uint16_t _num_labels;
    uint16_t _num_adj;
    uint16_t _num_sens;
    String _name;
 public:
    Page ();
    void Press(int x, int y, Adafruit_ILI9341 tft);
    void AddButton(Button2 &b);
    void AddLabel(Label2 l);
    void AddSensor(Sensor &s);
    void AddAdjuster(Adjuster a);
    void Show(Adafruit_ILI9341 tft);
    void Update(Adafruit_ILI9341 tft);
    void SetName(String name) { this->_name = name; }
    String GetName() {return this->_name; }
    Button2* GetButtons() {return this->_buttons; }
    Label2* GetLabels() {return this->_labels; }
    Adjuster* GetAdjusters() {return this->_adj; }

}; 


