#pragma once
#include <EEPROM.h>

// ID of the settings block
#define CONFIG_VERSION "sp1"

// Tell it where to store your config data in EEPROM
#define CONFIG_START 8

//settings structure
struct StoreStruct {
    char version[4];

    int CO2_setting, temp_setting, humid_setting;
    int waterLevel_setting, PH_setting;
    int light_on_min, light_on_hour;
    int light_off_min, light_off_hour;
    int current_mins, current_hours;
} SPstorage = {
    CONFIG_VERSION,
    // The default values
    10, 11, 12,
    13, 14,
    0, 6,
    0, 20,
    0, 10
};

void loadConfig() {
  // To make sure there are settings, and they are YOURS!
  // If nothing is found it will use the default settings.
  if (EEPROM.read(CONFIG_START + 0) == CONFIG_VERSION[0] &&
      EEPROM.read(CONFIG_START + 1) == CONFIG_VERSION[1] &&
      EEPROM.read(CONFIG_START + 2) == CONFIG_VERSION[2])
    for (unsigned int t=0; t<sizeof(SPstorage); t++)
      *((char*)&SPstorage + t) = EEPROM.read(CONFIG_START + t);
}

void saveConfig() {
  for (unsigned int t=0; t<sizeof(SPstorage); t++)
    EEPROM.write(CONFIG_START + t, *((char*)&SPstorage + t));
}


