#pragma once
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ILI9341.h>
#include <PLDuino.h>
#include <PLDTouch.h>
#include <page.h>
#include <elapsedMillis.h>
#include <EEPROM.h>
#include <load.h>




Adafruit_ILI9341 tft = Adafruit_ILI9341(PLDuino::LCD_CS, PLDuino::LCD_DC);
PLDTouch touch(PLDuino::TOUCH_CS, PLDuino::TOUCH_IRQ);
elapsedMillis timeElapsed;

bool light1_on = false; //annoying LED
bool light_on = false;
int pin_num = 39;
bool ctrl_on = true;
unsigned int update_interval = 200;
unsigned int current_mills = 0;
int current_secs = 0;

char time_as_str [7];
Page selectMenu = Page::Page();
Page menus[10];
Label2 * timer;
bool test_relays[4];
bool digital_outs[2];
Page* currentMenu = &selectMenu;
Button2 returnButton = Button2::Button2("Back", 230, 190, []() { 
  currentMenu = &selectMenu;
  currentMenu->Show(tft);
  delay(250);
  return;
 });

int CO2_setting, temp_setting, humid_setting;
int waterLevel_setting, PH_setting;
int light_on_min, light_on_hour;
int light_off_min, light_off_hour;
int current_mins, current_hours;





void setup() {
  // put your setup code here, to run once:
  PLDuino::init();
  loadConfig();

  // Power-on LCD and set it up
  PLDuino::enableLCD();
  tft.begin();
  tft.setRotation(3);
  touch.init(1);

  // Create Selection menu
  returnButton.SetHeight(40);
  returnButton.SetWidth(60);
  MakeSelectMenu();
  MakeRunMenu();
  MakeTestMenu();
  MakeAirCO2Menu();
  MakeAirTempMenu();
  MakeAirHumidityMenu();
  MakeWaterLevelMenu();
  MakeWaterPHMenu();
  MakeTimeMenu();
  MakeLightMenu();
  

  digitalWrite(13, 1); //turn the LED off
  digitalWrite(45, 1); //using for testing


  // initialize the screen
  currentMenu->Show(tft);

}

void MakeSelectMenu() {
  Label2 l1 = Label2::Label2("label", 95, 20);
  l1.SetLabel("Sky Project");
  l1.SetWidth(170);
  selectMenu.AddLabel(l1);

  Label2 l2 = Label2::Label2("label", 15, 100);
  l2.SetLabel(time_as_str);
  l2.SetWidth(120);
  l2.SetColor(ILI9341_OLIVE);
  selectMenu.AddLabel(l2);
  timer = &(selectMenu.GetLabels()[1]);
  
  Button2 btimer = Button2::Button2("time", 15, 100, []() { 
    currentMenu = &(menus[7]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
  btimer.SetWidth(120);
  btimer.SetHeight(50);
  selectMenu.AddButton(btimer);

  

  Button2 b1 = Button2::Button2("Run", 150, 100, []() { 
    currentMenu = &(menus[0]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
  selectMenu.AddButton(b1);

  Button2 b2 = Button2::Button2("Air", 15, 190, []() { 
    currentMenu = &(menus[2]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
  b2.SetWidth(90);
  b2.SetHeight(40);
  selectMenu.AddButton(b2);

  Button2 b3 = Button2::Button2("Light", 120, 190, []() { 
    currentMenu = &(menus[8]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
   b3.SetWidth(90);
   b3.SetHeight(40);
  selectMenu.AddButton(b3);

  Button2 b4 = Button2::Button2("Water", 225, 190, []() { 
    currentMenu = &(menus[5]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
   b4.SetWidth(90);
   b4.SetHeight(40);
  selectMenu.AddButton(b4);
}

void MakeRunMenu() {
  Label2 l1 = Label2::Label2("Run", 105, 20);
  l1.SetWidth(100);
  menus[0].AddLabel(l1);

  Button2 b1 = Button2::Button2("Start", 30, 100, []() { 
    ctrl_on = true;
    return;
   });
  menus[0].AddButton(b1);

  Button2 b2 = Button2::Button2("Stop", 170, 100, []() { 
    ctrl_on = false;
    test_relays[0] = false;
    test_relays[1] = false;
    test_relays[2] = false;
    test_relays[3] = false;
    digitalWrite(PLDuino::RELAY1, LOW);
    digitalWrite(PLDuino::RELAY2, LOW);
    digitalWrite(PLDuino::RELAY3, LOW);
    digitalWrite(PLDuino::RELAY4, LOW);
    return;
   });
  menus[0].AddButton(b2);

  Button2 b3 = Button2::Button2("Test", 110, 190, []() { 
    currentMenu = &(menus[1]);
    currentMenu->Show(tft);
    delay(250);
    return;
   });
  b3.SetWidth(90);
  b3.SetHeight(40);
  menus[0].AddButton(b3);

  menus[0].AddButton(returnButton);
}

void MakeTestMenu() {
  Label2 l1 = Label2::Label2("Test", 105, 20);
  l1.SetWidth(100);
  menus[1].AddLabel(l1);

  Button2 b1 = Button2::Button2("Fan", 30, 100, []() { 
    if (test_relays[0]) {
      test_relays[0] = false;
      digitalWrite(PLDuino::RELAY1, LOW);
      (menus[1].GetButtons())[0].SetColor(ILI9341_RED);
    } else {
      test_relays[0] = true;
      digitalWrite(PLDuino::RELAY1, HIGH);
      (menus[1].GetButtons())[0].SetColor(ILI9341_GREEN);
    }
    (menus[1].GetButtons())[0].Show(tft);
    delay(250);
    return;
   });
   b1.SetColor(ILI9341_RED);
   test_relays[0] = false;
   b1.SetWidth(80);
  menus[1].AddButton(b1);

  Button2 b2 = Button2::Button2("Pump", 140, 100, []() { 
    if (test_relays[1]) {
      test_relays[1] = false;
      digitalWrite(PLDuino::RELAY2, LOW);
      (menus[1].GetButtons())[1].SetColor(ILI9341_RED);
    } else {
      test_relays[1] = true;
      digitalWrite(PLDuino::RELAY2, HIGH);
      (menus[1].GetButtons())[1].SetColor(ILI9341_GREEN);
    }
    (menus[1].GetButtons())[1].Show(tft);
    delay(250);
    return;
   });
   b2.SetColor(ILI9341_RED);
   test_relays[1] = false;
   b2.SetWidth(80);
  menus[1].AddButton(b2);

  Button2 b3 = Button2::Button2("Light", 30, 190, []() { 
    if (test_relays[2]) {
      test_relays[2] = false;
      digitalWrite(PLDuino::RELAY3, LOW);
      (menus[1].GetButtons())[2].SetColor(ILI9341_RED);
    } else {
      test_relays[2] = true;
      digitalWrite(PLDuino::RELAY3, HIGH);
      (menus[1].GetButtons())[2].SetColor(ILI9341_GREEN);
    }
    (menus[1].GetButtons())[2].Show(tft);
    delay(250);
    return;
   });
   b3.SetColor(ILI9341_RED);
   test_relays[2] = false;
   b3.SetWidth(80);
   menus[1].AddButton(b3);

   Button2 b4 = Button2::Button2("CO2", 140, 190, []() { 
    if (test_relays[3]) {
      test_relays[3] = false;
      digitalWrite(PLDuino::RELAY4, LOW);
      (menus[1].GetButtons())[3].SetColor(ILI9341_RED);
    } else {
      test_relays[3] = true;
      digitalWrite(PLDuino::RELAY4, HIGH);
      (menus[1].GetButtons())[3].SetColor(ILI9341_GREEN);
    }
    (menus[1].GetButtons())[3].Show(tft);
    delay(250);
    return;
   });
   b4.SetWidth(80);
   b4.SetColor(ILI9341_RED);
   test_relays[3] = false;
  menus[1].AddButton(b4);

  menus[1].AddButton(returnButton);
}

void MakeAirCO2Menu() {
  Label2 l1 = Label2::Label2("Air:CO2", 105, 20);
  l1.SetWidth(100);
  menus[2].AddLabel(l1);

  Sensor s1 = Sensor(0,"", 120, 90);
  menus[2].AddSensor(s1);

  Label2 l3 = Label2::Label2("current:", 20, 90);
  l3.SetWidth(100);
  l3.SetHeight(35);
  l3.SetColor(ILI9341_DARKCYAN);
  menus[2].AddLabel(l3);

  
  Adjuster a1 = Adjuster(&(SPstorage.CO2_setting), 120, 160);
  menus[2].AddAdjuster(a1);
  
  Label2 l4 = Label2::Label2("setting:", 20, 160);
  l4.SetWidth(100);
  l4.SetHeight(35);
  l4.SetColor(ILI9341_DARKCYAN);
  menus[2].AddLabel(l4);

  Label2 l2 = Label2::Label2("off", 230, 20);
  l2.SetWidth(50);
  l2.SetStatic(false);
  menus[2].AddLabel(l2);

  Button2 b1 = Button2("Next", 230, 135, []() {
    currentMenu = &(menus[3]);
    currentMenu->Show(tft);
    delay(250);
    return;
  });
  b1.SetHeight(40);
  b1.SetWidth(60);
  menus[2].AddButton(b1);

  menus[2].AddButton(returnButton);
}

void MakeAirTempMenu() {
  Label2 l1 = Label2::Label2("Air:Temp", 100, 20);
  l1.SetWidth(110);
  menus[3].AddLabel(l1);

  Sensor s1 = Sensor(1,"", 120, 90);
  menus[3].AddSensor(s1);

  Label2 l3 = Label2::Label2("current:", 20, 90);
  l3.SetWidth(100);
  l3.SetHeight(35);
  l3.SetColor(ILI9341_DARKCYAN);
  menus[3].AddLabel(l3);


  Adjuster a1 = Adjuster(&(SPstorage.temp_setting), 120, 160);
  menus[3].AddAdjuster(a1);
  
  Label2 l4 = Label2::Label2("setting:", 20, 160);
  l4.SetWidth(100);
  l4.SetHeight(35);
  l4.SetColor(ILI9341_DARKCYAN);
  menus[3].AddLabel(l4);

  Label2 l2 = Label2::Label2("off", 230, 20);
  l2.SetWidth(50);
  l2.SetStatic(false);
  menus[3].AddLabel(l2);

  Button2 b1 = Button2("Next", 230, 135, []() {
    currentMenu = &(menus[4]);
    currentMenu->Show(tft);
    delay(250);
    return;
  });
  b1.SetHeight(40);
  b1.SetWidth(60);
  menus[3].AddButton(b1);

  menus[3].AddButton(returnButton);
}

void MakeAirHumidityMenu() {
  Label2 l1 = Label2::Label2("Air:Humid", 95, 20);
  l1.SetWidth(120);
  menus[4].AddLabel(l1);

  Sensor s1 = Sensor(2,"", 120, 90);
  menus[4].AddSensor(s1);

  Label2 l3 = Label2::Label2("current:", 20, 90);
  l3.SetWidth(100);
  l3.SetHeight(35);
  l3.SetColor(ILI9341_DARKCYAN);
  menus[4].AddLabel(l3);


  Adjuster a1 = Adjuster(&(SPstorage.humid_setting), 120, 160);
  menus[4].AddAdjuster(a1);
  
  Label2 l4 = Label2::Label2("setting:", 20, 160);
  l4.SetWidth(100);
  l4.SetHeight(35);
  l4.SetColor(ILI9341_DARKCYAN);
  menus[4].AddLabel(l4);

  Label2 l2 = Label2::Label2("off", 230, 20);
  l2.SetWidth(50);
  l2.SetStatic(false);
  menus[4].AddLabel(l2);



  menus[4].AddButton(returnButton);
}

void MakeWaterLevelMenu() {
  Label2 l1 = Label2::Label2("Water:Level", 85, 20);
  l1.SetWidth(140);
  menus[5].AddLabel(l1);

  Sensor s1 = Sensor(3,"", 120, 90);
  menus[5].AddSensor(s1);

  Label2 l3 = Label2::Label2("current:", 20, 90);
  l3.SetWidth(100);
  l3.SetHeight(35);
  l3.SetColor(ILI9341_DARKCYAN);
  menus[5].AddLabel(l3);


  Adjuster a1 = Adjuster(&(SPstorage.waterLevel_setting), 120, 160);
  menus[5].AddAdjuster(a1);
  
  Label2 l4 = Label2::Label2("setting:", 20, 160);
  l4.SetWidth(100);
  l4.SetHeight(35);
  l4.SetColor(ILI9341_DARKCYAN);
  menus[5].AddLabel(l4);

  Label2 l2 = Label2::Label2("off", 230, 20);
  l2.SetWidth(50);
  l2.SetStatic(false);
  menus[5].AddLabel(l2);

  Button2 b1 = Button2("Next", 230, 135, []() {
    currentMenu = &(menus[6]);
    currentMenu->Show(tft);
    delay(250);
    return;
  });
  b1.SetHeight(40);
  b1.SetWidth(60);
  menus[5].AddButton(b1);

  menus[5].AddButton(returnButton);
}

void MakeWaterPHMenu() {
  Label2 l1 = Label2::Label2("Water:PH", 100, 20);
  l1.SetWidth(110);
  menus[6].AddLabel(l1);

  Sensor s1 = Sensor(4,"", 120, 90);
  menus[6].AddSensor(s1);

  Label2 l3 = Label2::Label2("current:", 20, 90);
  l3.SetWidth(100);
  l3.SetHeight(35);
  l3.SetColor(ILI9341_DARKCYAN);
  menus[6].AddLabel(l3);


  Adjuster a1 = Adjuster(&(SPstorage.PH_setting), 120, 160);
  menus[6].AddAdjuster(a1);
  
  Label2 l4 = Label2::Label2("setting:", 20, 160);
  l4.SetWidth(100);
  l4.SetHeight(35);
  l4.SetColor(ILI9341_DARKCYAN);
  menus[6].AddLabel(l4);

  Label2 l2 = Label2::Label2("good", 225, 20);
  l2.SetWidth(65);
  l2.SetStatic(false);
  menus[6].AddLabel(l2);


  menus[6].AddButton(returnButton);
}

void MakeTimeMenu() {
  Label2 l1 = Label2::Label2("Current Time", 90, 20);
  l1.SetWidth(150);
  menus[7].AddLabel(l1);

  Adjuster a1 = Adjuster(&(SPstorage.current_mins), 154, 160);
  menus[7].AddAdjuster(a1);
  Adjuster a2 = Adjuster(&(SPstorage.current_hours), 100, 160);
  menus[7].AddAdjuster(a2);

  menus[7].AddButton(returnButton);
}

void MakeLightMenu() {
  Label2 l1 = Label2::Label2("Light on", 10, 20);
  l1.SetWidth(110);
  menus[8].AddLabel(l1);

  Adjuster a1 = Adjuster(&(SPstorage.light_on_min), 64, 110);
  menus[8].AddAdjuster(a1);
  Adjuster a2 = Adjuster(&(SPstorage.light_on_hour), 10, 110);
  menus[8].AddAdjuster(a2);

  Label2 l2 = Label2::Label2("Light off", 150, 20);
  l2.SetWidth(110);
  menus[8].AddLabel(l2);

  Adjuster a3 = Adjuster(&(SPstorage.light_off_min), 204, 110);
  menus[8].AddAdjuster(a3);
  Adjuster a4 = Adjuster(&(SPstorage.light_off_hour), 150, 110);
  menus[8].AddAdjuster(a4);


  menus[8].AddButton(returnButton);
}

char buffer [5];

int sign(int x)
{
    if(x > 0) return 1;
    if(x < 0) return -1;
    return 0;
}


void loop() {
  
  if (touch.dataAvailable())
  {
    Point pt = touch.read();
    currentMenu->Press(pt.x, pt.y, tft);
    saveConfig();
  }
  if (timeElapsed > update_interval) 
	{				
    if (!(currentMenu == &menus[1]) && ctrl_on) { //disable behavior while in test menu
      int CO2_reading = analogRead(0);
      if (CO2_reading < SPstorage.CO2_setting) {
        if (test_relays[3]) {
          //CO2 solenoid already on
        } else {
          test_relays[3] = true;
          digitalWrite(PLDuino::RELAY4, HIGH);
          (menus[1].GetButtons())[3].SetColor(ILI9341_GREEN);
          (menus[2].GetLabels())[3].SetColor(ILI9341_GREEN);
          (menus[2].GetLabels())[3].SetLabel("on");
        }
      }
      else {
        if (!test_relays[3]) {
          //CO2 solenoid already off
        } else {
          test_relays[3] = false;
          digitalWrite(PLDuino::RELAY4, LOW);
          (menus[1].GetButtons())[3].SetColor(ILI9341_RED);
          (menus[2].GetLabels())[3].SetColor(ILI9341_RED);
          (menus[2].GetLabels())[3].SetLabel("off");
        }
      }

      int temp_reading = analogRead(1);
      if (temp_reading > SPstorage.temp_setting) {
        if (test_relays[0]) {
          //fan already on
        } else {
          test_relays[0] = true;
          digitalWrite(PLDuino::RELAY1, HIGH);
          (menus[1].GetButtons())[0].SetColor(ILI9341_GREEN);
          (menus[3].GetLabels())[3].SetColor(ILI9341_GREEN);
          (menus[3].GetLabels())[3].SetLabel("on");
        }
      }
      else {
        if (!test_relays[0]) {
          //fan already off
        } else {
          test_relays[0] = false;
          digitalWrite(PLDuino::RELAY1, LOW);
          (menus[1].GetButtons())[0].SetColor(ILI9341_RED);
          (menus[3].GetLabels())[3].SetColor(ILI9341_RED);
          (menus[3].GetLabels())[3].SetLabel("off");
        }
      }

      int humid_reading = analogRead(2);
      if (humid_reading < SPstorage.humid_setting) {
        if (digital_outs[0]) {
          //mister already on
        } else {
          digital_outs[0] = true;
          //TURN MISTER ON

          (menus[4].GetLabels())[3].SetColor(ILI9341_GREEN);
          (menus[4].GetLabels())[3].SetLabel("on");
        }
      }
      else {
        if (!digital_outs[0]) {
          //mister already off
        } else {
          digital_outs[0] = false;
          //TURN MISTER OFF 
          (menus[4].GetLabels())[3].SetColor(ILI9341_RED);
          (menus[4].GetLabels())[3].SetLabel("off");
        }
      }

      int waterLevel_reading = analogRead(3);
      if (waterLevel_reading < SPstorage.waterLevel_setting) {
        if (test_relays[1]) {
          //pump already on
        } else {
          test_relays[1] = true;
          digitalWrite(PLDuino::RELAY2, HIGH);
          (menus[1].GetButtons())[1].SetColor(ILI9341_GREEN);
          (menus[5].GetLabels())[3].SetColor(ILI9341_GREEN);
          (menus[5].GetLabels())[3].SetLabel("on");
        }
      }
      else {
        if (!test_relays[1]) {
          //pump already off
        } else {
          test_relays[1] = false;
          digitalWrite(PLDuino::RELAY2, LOW);
          (menus[1].GetButtons())[1].SetColor(ILI9341_RED);
          (menus[5].GetLabels())[3].SetColor(ILI9341_RED);
          (menus[5].GetLabels())[3].SetLabel("off");
        }
      }

      int waterPH_reading = analogRead(4);
      if (waterPH_reading < SPstorage.PH_setting) {
        if (digital_outs[1]) {
          //  already on
        } else {
          digital_outs[1] = true;
          //  turn ON

          (menus[6].GetLabels())[3].SetColor(ILI9341_GREEN);
          (menus[6].GetLabels())[3].SetLabel("bad");
        }
      }
      else {
        if (!digital_outs[1]) {
          //mister already off
        } else {
          digital_outs[1] = false;
          //TURN MISTER OFF 
          (menus[6].GetLabels())[3].SetColor(ILI9341_RED);
          (menus[6].GetLabels())[3].SetLabel("good");
        }
      }

      int current_time = SPstorage.current_hours * 60 + SPstorage.current_mins;
      bool dayLighted = SPstorage.light_on_hour * 60 + SPstorage.light_on_min < SPstorage.light_off_hour * 60 + SPstorage.light_off_min;
      if ((sign(current_time - SPstorage.light_on_hour * 60 + SPstorage.light_on_min) == sign(current_time - SPstorage.light_off_hour * 60 + SPstorage.light_off_min)) ^ dayLighted) {
        //lights on
        if (!light_on) {
          timer->SetColor(ILI9341_YELLOW);
          light_on = true;
          digitalWrite(PLDuino::RELAY3, HIGH);
        }
        //do nothing
      }
      else {
        //lights off
        if (light_on) {
          timer->SetColor(ILI9341_OLIVE);
          light_on = false;
          digitalWrite(PLDuino::RELAY3, LOW);
          //turn it off
        }
        //do nothign
      }
  }
    

    currentMenu->Update(tft);
    timer->SetStatic(true);
    updateTime(timeElapsed);
    
		timeElapsed = 0;			 // reset the counter to 0
	}
}

void updateTime(unsigned int elapsed) {
  current_mills += elapsed;
  
  if (current_mills >= 1000) {
    timer->SetStatic(false);
    current_mills -= 1000;
    current_secs ++;
  }

  if (current_secs >=60) {
    SPstorage.current_mins ++;
    saveConfig();
    current_secs -= 60;
  }

  if (SPstorage.current_mins >= 60) {
    SPstorage.current_mins -= 60;
    SPstorage.current_hours ++;
  }

  if (SPstorage.current_hours >= 24 ) {
    SPstorage.current_hours -= 24;
  }

  strcpy(time_as_str, "000000");
  
  itoa(current_secs, buffer, 10);
  auto len = strlen(buffer);
  memcpy(&time_as_str[6-len] ,buffer, len );

  itoa(SPstorage.current_mins, buffer, 10);
  len = strlen(buffer);
  memcpy(&time_as_str[4-len] ,buffer, len );

  itoa(SPstorage.current_hours, buffer, 10);
  len = strlen(buffer);
  memcpy(&time_as_str[2-len] ,buffer, len );

}