

These files are for programming the "open source PLC" arduino for use with the sky project. 

I am using the arduino 1.8.5 software to compile and upload. (set the "sketchbook" to ...\Sky_Project\Arduino then upload "MainProgram") 



Analog input and Relay connections
----------------------------------------
CO2_reading = analogRead(0); C02 Solenoid = RELAY4
temp_reading = analogRead(1); Fan = RELAY1
humid_ reading = analogRead(2); Mister = digital_outs[0]
waterLevel_reading = analogRead(3); Water pump = RELAY2
waterPH_reading = analogRead(4); no PH mechanism
Light relay = RELAY3
